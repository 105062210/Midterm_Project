# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. User Page
    2. Post Page
    3. Post List Page
    4. leave comment under any post
* Other functions (add/delete)
    1. vote
    2. personal configuration


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
一個類似Instagram（以下簡稱IG）的社群網站，可以分享照片貼文，而其他人可以按讚留言，另外
跟IG不同的是在User page中你可以翻轉照片來隱藏，也可再次翻轉秀出照片，增添了娛樂性。
## Security Report (Optional)
