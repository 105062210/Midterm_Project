var storageRef = firebase.storage().ref();
var database = firebase.database();
var changeFileName = '';
var newUser = false;
var PicRef = null;

window.onload = function() {
  var userName = document.getElementById('userName');
  var selfDescription = document.getElementById('selfDescription');
  firebase.auth().onAuthStateChanged((user) => {

    database.ref('users/' + user.uid).once('value').then((snapshot) => {
      if (snapshot.exists()) {
        console.log('welcome back ' + user.displayName);
        var _user = snapshot.val();
        userName.value = _user['name'];
        selfDescription.value = _user['selfDescription'];
        if (_user['head_picture'])
        document.getElementById('Pic').src = _user['head_picture'];
      } else {
        newUser = true;
        console.log('You are a new user!');
      }
    });
  });

  var changeHeadPicArea = document.getElementById('changeHeadPic');
  changeHeadPicArea.children[0].style.display = 'none';
}

function uploadHeadPic(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var pic = this.document.getElementById('Pic');
      pic.src = e.target.result;
      pic.width = 300;
      pic.height = 300;
    };
    reader.readAsDataURL(input.files[0]);
    changeFileName = input.files[0].name;
  }
}

function changeHeadPic() {
  var changeHeadPicArea = document.getElementById('changeHeadPic');
  if (changeHeadPicArea.children[0].style.display === 'none')
    changeHeadPicArea.children[0].style.display = 'inherit';
  else
    changeHeadPicArea.children[0].style.display = 'none';
}

function createNewUser(user, url) {
  var userName = document.getElementById('userName').value;
  var selfDescription = document.getElementById('selfDescription').value;
  if (userName === '' || selfDescription === '') {
    console.log('please fill in the userName or selfDescription field');
    return;
  }

  var newUserRef = database.ref('users/' + user.uid);
  console.log(user);
  newUserRef.set({
    email: user.email,
    head_picture: url,
    name: userName,
    profile_picture: {},
    selfDescription: selfDescription
  }).then(() => {
    console.log('save the user info.');
    if (newUser)
      location.href = 'userPage.html';
  });
}

function save() {
    firebase.auth().onAuthStateChanged((user) => {
      if (changeFileName !== '') {
        PicRef = storageRef.child(changeFileName);
        if (!user) return;

        var PicBase64Url = document.getElementById('Pic').src;
        PicRef.putString(PicBase64Url, 'data_url').then((snapshot) => {
          console.log('Uploaded a data_url string!');
          PicRef.getDownloadURL().then(function(url) {
            console.log('uploaded: ' + url);
            if (newUser && url !== '') {
              console.log('create an user data');
              createNewUser(user, url);
              return;
            }
            var userName = document.getElementById('userName').value;
            var selfDescription = document.getElementById('selfDescription').value;

            database.ref('users/' + user.uid).update({
              head_picture: url,
              name: userName,
              selfDescription: selfDescription
            });
            console.log('update the user info.');
            if (newUser)
              location.href = 'userPage.html';
          }).catch(function(error) {
              // A full list of error codes is available at
              // https://firebase.google.com/docs/storage/web/handle-errors
              switch (error.code) {
                case 'storage/object_not_found':
                  // File doesn't exist
                  break;

                case 'storage/unauthorized':
                  // User doesn't have permission to access the object
                  break;

                case 'storage/canceled':
                  // User canceled the upload
                  break;

                case 'storage/unknown':
                  // Unknown error occurred, inspect the server response
                  break;
              }
            });
        });
      } else {
        var userName = document.getElementById('userName').value;
        var selfDescription = document.getElementById('selfDescription').value;

        database.ref('users/' + user.uid).update({
          name: userName,
          selfDescription: selfDescription
        });
        console.log('update the user info.');
        if (newUser)
          location.href = 'userPage.html';
      }
    });
}
