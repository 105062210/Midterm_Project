var database = firebase.database();

window.onload = function() {
  var _posts = document.getElementById('posts');

    database.ref('posts').once('value').then((snapshot) => {
      var posts = snapshot.val();
      for (var key in posts) {
        var post = posts[key];
          _posts.innerHTML += `
          <div class="card post" style="width: 18rem;">
            <a class="card-title post-title" href="#">&nbsp;&nbsp;` + post['posterName'] + `</a>
            <img class="card-img-top" src=` + post['url'] + ` alt="Card image cap" id=` + key + ` onclick="enterPost(event)">
            <div class="card-body">
              <p class="card-text">` + post['description'] + `</p>
            </div>
          </div>
          `;
      }
    });

  firebase.auth().onAuthStateChanged((user) => {
    var user_email = '';
    var menu = document.getElementById('dynamic-menu');
    // Check user login
    if (user) {
        user_email = user.email;
        menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        /// TODO 5: Complete logout button event
        ///         1. Add a listener to logout button
        ///         2. Show alert when logout success or error (use "then & catch" syntex)
        var logout_btn = document.getElementById('logout-btn');
        if (logout_btn === null) console.log('NULL');
        else console.log('Not NULL');
        document.getElementById('logout-btn').addEventListener('click', function() {
          firebase.auth().signOut().then(() => {
            alert('logout success');
          }).catch((err) => {
            alert(err.message);
          });
        });
    } else {
        // It won't show any post if not login
        menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
    }
  });
}

function enterPost(e) {
  sessionStorage.setItem('postId', e.target.id);
  window.setTimeout('location.href = "post.html";', 300);
}
