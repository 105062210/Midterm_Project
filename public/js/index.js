function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logout_btn = document.getElementById('logout-btn');
            if (logout_btn === null) console.log('NULL');
            else console.log('Not NULL');
            document.getElementById('logout-btn').addEventListener('click', function() {
              firebase.auth().signOut().then(() => {
                alert('logout success');
              }).catch((err) => {
                alert(err.message);
              });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });
}

window.onload = function () {
    init();
};
