

var database = firebase.database();
var postId = '';


window.onload = function() {
  postId = sessionStorage.getItem('postId');
  if (!postId) return;
  sessionStorage.removeItem('postId');
  console.log(postId);

  firebase.auth().onAuthStateChanged((user) => {
  database.ref('posts/' + postId).once('value').then((snapshot) => {
    var post = snapshot.val();
    var picImg = this.document.getElementById('picImg');
    var description = this.document.getElementById('description');
    var poster = this.document.getElementById('poster');
    var posterDate = this.document.getElementById('postDate');

    picImg.src = post['url'];
    picImg.width = 300;
    picImg.height = 300;
    description.innerHTML = post['description'];
    poster.innerHTML = post['posterName'];
    postDate.innerHTML = 'Date: ' + new Date(post['uploadDate']).toLocaleDateString();

    var vote = document.getElementById('vote');
    var voters = snapshot.val()['voters'];
    if (!voters) {
      vote.innerHTML = `
      <span id="voteGood"><i id="empty" class="far fa-moon fa-2x" onclick="voteGood()"></i></span>
      <span id="voteBad"><i id="empty" class="far fa-lemon fa-2x" onclick="voteBad()"></i></span>
      `;
    } else {
      for (var key in voters) {
        var voter = voters[key];
        if (voter['voterId'] === user.uid) {
          if (voter['voteType'] === 'good') {
            vote.innerHTML = `
            <span id="voteGood"><i id="full" class="fas fa-moon fa-2x" onclick="voteGood()"></i></span>
            `;
          } else {
            vote.innerHTML = `
            <span id="voteBad"><i id="full" class="fas fa-lemon fa-2x" onclick="voteBad()"></i></span>
            `;
          }
        } else {
          vote.innerHTML = `
          <span id="voteGood"><i id="empty" class="far fa-moon fa-2x" onclick="voteGood()"></i></span>
          <span id="voteBad"><i id="empty" class="far fa-lemon fa-2x" onclick="voteBad()"></i></span>
          `;
          break;
        }
      }
    }


    for(var key in post['comments']) {
      var comment = post['comments'][key];
      var commentList = document.getElementById('comments');
      commentList.innerHTML += `
        <li class="comment">
          <a class="commenter" href="#" >` + comment.commenter + `:&nbsp;</a>
          <span>` + comment.commentContent + `</span>
        </li>
      `;
    }
    console.log(post);
  });
  });
}


function makeComment() {
  firebase.auth().onAuthStateChanged((user) => {
    if (!user) return;
    database.ref('users/' + user.uid).once('value').then((snapshot) => {
      var userName = snapshot.val()['name'];
      var input = document.getElementById('commentInput').value;
      var commentList = document.getElementById('comments');
      var newCommentKey = database.ref('posts/' + postId + '/comments').push().key;
      database.ref('posts/' + postId + '/comments/' + newCommentKey).set({
        commenter: userName,
        commentDate: new Date(),
        commentContent: input
      });
      commentList.innerHTML += `
        <li class="comment">
          <a class="commenter" href="#" >` + userName + `:&nbsp;</a>
          <span>` + input + `</span>
        </li>
      `;
    });
  });
}

function voteGood() {
  var voteIcon = this.document.getElementById('voteGood');
  var voteArea = this.document.getElementById('vote');

  firebase.auth().onAuthStateChanged((user) => {
    if (voteIcon.children[0].id === 'empty') { // you have not yet vote good on this post and give a first good vote
      voteArea.innerHTML = `
      <span id="voteGood"><i id="full" class="fas fa-moon fa-2x" onclick="voteGood()"></i></span>
      `;

        database.ref('posts/' + postId).once('value').then((snapshot) => {
          var updates = {};
          var goodVoter = snapshot.val()['voters/good'];
          if (!goodVoter) { // first vote good on this post
            var newVoterKey = database.ref('posts/' + postId + '/voters').push().key;
            updates['voters/' + newVoterKey] = {
              voterId: user.uid,
              voteType: 'good'
            };
            var goodVoteNum = snapshot.val()['votes']['good'];
            updates['votes'] = {
              good: goodVoteNum + 1,
              bad: snapshot.val()['votes']['bad']
            };
            database.ref('posts/' + postId + '/').update(updates);
          }
        });

    }
    else { // you have been voted good on this post and want to take it back
      voteArea.innerHTML = `
      <span id="voteGood"><i id="empty" class="far fa-moon fa-2x" onclick="voteGood()"></i></span>
      <span id="voteBad"><i id="empty" class="far fa-lemon fa-2x" onclick="voteBad()"></i></span>
      `;

      database.ref('posts/' + postId).once('value').then((snapshot) => {
        var voters = snapshot.val()['voters'];
        for (var key in voters) {
          var voter = voters[key];
          if (voter['voterId'] === user.uid) {
            if (voter['voteType'] === 'good') {
              var goodVoteNum = snapshot.val()['votes']['good'];
              database.ref('posts/' + postId + '/votes').update({
                good: goodVoteNum - 1,
                bad: snapshot.val()['votes']['bad']
              });
            } else {
              console.error('The vote type is mess up');
            }
            database.ref('posts/' + postId + '/voters/' + key).remove();
            break;
          } else {
            console.error('The voter has not found!');
            break;
          }
        }
      });
    }
  });
}

function voteBad() {
  var voteIcon = this.document.getElementById('voteBad');
  var voteArea = this.document.getElementById('vote');

  firebase.auth().onAuthStateChanged((user) => {
    if (voteIcon.children[0].id === 'empty') {
      voteArea.innerHTML = `
        <span id="voteBad"><i id="full" class="fas fa-lemon fa-2x" onclick="voteBad()"></i></span>
      `;

      database.ref('posts/' + postId).once('value').then((snapshot) => {
        var updates = {};
        var badVoter = snapshot.val()['voters/bad'];
        if (!badVoter) { // first vote good on this post
          var newVoterKey = database.ref('posts/' + postId + '/voters').push().key;
          updates['voters/' + newVoterKey] = {
            voterId: user.uid,
            voteType: 'bad'
          };
          var badVoteNum = snapshot.val()['votes']['bad'];
          updates['votes'] = {
            good: snapshot.val()['votes']['good'],
            bad: badVoteNum + 1
          };
          database.ref('posts/' + postId + '/').update(updates);
        }
      });
    }
    else {
      voteArea.innerHTML = `
      <span id="voteGood"><i id="empty" class="far fa-moon fa-2x" onclick="voteGood()"></i></span>
      <span id="voteBad"><i id="empty" class="far fa-lemon fa-2x" onclick="voteBad()"></i></span>
      `;

      database.ref('posts/' + postId).once('value').then((snapshot) => {
        var voters = snapshot.val()['voters'];
        for (var key in voters) {
          var voter = voters[key];
          if (voter['voterId'] === user.uid) {
            if (voter['voteType'] === 'bad') {
              var badVoteNum = snapshot.val()['votes']['bad'];
              database.ref('posts/' + postId + '/votes').update({
                good: snapshot.val()['votes']['good'],
                bad: badVoteNum - 1
              });
            } else {
              console.error('The vote type is mess up');
            }
            database.ref('posts/' + postId + '/voters/' + key).remove();
            break;
          } else {
            console.error('The voter has not found!');
            break;
          }
        }
      });
    }
  });
}
