var selfPic;

// Create a storage reference from our storage service
var storageRef = firebase.storage().ref();

// Child references can also take paths delimited by '/'


var database = firebase.database();

window.onload = function() {
  selfPic = document.getElementById('selfPic');
  headPic = document.getElementById('headPic');
  selfIntro = document.getElementById('selfIntro');

  firebase.auth().onAuthStateChanged((user) => {
    if (!user) return;
    database.ref('users/' + user.uid).once('value').then(function(snapshot) {
      if (!snapshot.exists())
        location.href = 'configPage.html';
      var headPicUrl = snapshot.val()['head_picture'];
      headPic.src = headPicUrl;
      selfIntro.innerHTML = snapshot.val()['selfDescription'];
      var postIds =  snapshot.val()['profile_picture'];
      console.log(postIds);
      for (var _postId in postIds) {
          database.ref('posts/' + postIds[_postId]).once('value').then(function(snapshot) {
            var url = snapshot.val()['url'];
            selfPic.innerHTML += `
              <div class="Pic" onclick="showImg(event)" ondblclick="enterPost(event)">
                <img class="PicImg animated flipInY" src=` + url + ` alt="" onclick="hiddenImg(event)" id=` + snapshot.ref.path.n[1] + `>
              </div>`;
          });
      }

      var user_email = '';
      var menu = document.getElementById('dynamic-menu');
      // Check user login
      if (user) {
          user_email = user.email;
          menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
          /// TODO 5: Complete logout button event
          ///         1. Add a listener to logout button
          ///         2. Show alert when logout success or error (use "then & catch" syntex)
          var logout_btn = document.getElementById('logout-btn');
          if (logout_btn === null) console.log('NULL');
          else console.log('Not NULL');
          document.getElementById('logout-btn').addEventListener('click', function() {
            firebase.auth().signOut().then(() => {
              alert('logout success');
            }).catch((err) => {
              alert(err.message);
            });
          });
      } else {
          // It won't show any post if not login
          menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
      }
    });
  });
}



function showAllImg() {
  var pics = document.getElementsByClassName('Pic');
  for (let i = 0; i < pics.length; i++) {
    pics[i].style.background = '';
    pics[i].children[0].style.visibility = 'visible';
    pics[i].children[0].className = "PicImg animated flipInY";
  }
}

function hiddenAllImg() {
  var pics = document.getElementsByClassName('Pic');
  var picImgs = document.getElementsByClassName('PicImg');
  for (let i = 0; i < picImgs.length; i++) {
    pics[i].style.background = 'black';
    picImgs[i].style.visibility = 'hidden';
    picImgs[i].className = 'PicImg animated flipOutY';
  }
}

function showImg(e) {
  if (e.target.children[0]) {
    e.target.style.background = '';
    e.target.children[0].style.visibility = 'visible';
    e.target.children[0].className = 'PicImg animated flipInY';
  }
}

function hiddenImg(e) {
  if (e.target.parentNode.style.background === '') {
    e.target.parentNode.style.background = 'black';
    e.target.style.visibility = 'hidden';
    e.target.className = 'PicImg animated flipOutY';
  }
}

function configure() {
  location.href = "configPage.html";
}

function newPost() {
  location.href = "newPost.html";
}

function enterPost(e) {
  if (e.target.className === 'Pic') {
    console.log(e.target.children[0].id);
    sessionStorage.setItem('postId', e.target.children[0].id);
  } else {
    console.log(e.target.id);
    sessionStorage.setItem('postId', e.target.id);
  }
  window.setTimeout('location.href = "post.html";', 300);
}
