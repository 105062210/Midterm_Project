var storageRef = firebase.storage().ref();
var database = firebase.database();
var fileName = '';
function uploadPic(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var pic = document.getElementById('Pic');
      pic.src = e.target.result;
      pic.width = 300;
      pic.height = 300;
    };
    reader.readAsDataURL(input.files[0]);
    fileName = input.files[0].name;
  }
  // e.target.parentNode.innerHTML += `<img src=` + e.target.value + `/>`;
}

function submitPost() {

firebase.auth().onAuthStateChanged((user) => {
  var PicBase64Url = document.getElementById('Pic').src;
  var descriptionText = document.getElementById('description').value;

    if (fileName === '') return;
    var PicRef = storageRef.child(fileName);
    PicRef.putString(PicBase64Url, 'data_url').then((snapshot) => {
      console.log('Uploaded a data_url string!');
      PicRef.getDownloadURL().then(function(url) {
        database.ref('users/' + user.uid).once('value').then((snapshot) => {
          var userName = snapshot.val().name;
          var newPostKey = database.ref('posts/').push().key;
          var newProfile_pictureKey = database.ref('users/' + user.uid + '/profile_picture').push().key;
          var updates = {};

          updates['posts/' + newPostKey] = {
            description: descriptionText,
            posterName: userName,
            uploadDate: new Date(),
            url: url,
            userId: user.uid,
            votes: {
                bad: 0,
                good: 0
            }
          };
          updates['users/' + user.uid + '/profile_picture/' + newProfile_pictureKey] = newPostKey;

          firebase.database().ref().update(updates).then((snapshot) => {
            console.log('save data successfully!');
            setTimeout('location.href = "userPage.html";', 300);
          }).catch((err) => {
            console.log(err.message);
          });
        });

      }).catch(function(error) {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
            case 'storage/object_not_found':
              // File doesn't exist
              break;

            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
              // Unknown error occurred, inspect the server response
              break;
          }
        });
    });
  });
}
